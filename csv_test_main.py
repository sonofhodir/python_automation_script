import csv
import glob
import os
import re
import sys

import pandas as pd

def populateDataFrameWithLogic(file_name, out_file):
    # remove empty lines
    out_file_name = file_name.replace(".csv", "_cleansed.csv")
    with open(file_name) as in_file:
        with open(out_file_name, 'w') as out_file_w:
            writer = csv.writer(out_file_w)
            for row in csv.reader(in_file):
                if any(row):
                    writer.writerow(row)

    df = pd.read_csv(out_file_name)

    # rename column based on index position
    df.rename(columns={df.columns[0]: "Fund Name"}, inplace=True)

    # extract and assign value into new column

    df['SEDOL'] = df.astype(str).sum(axis=1).str.extract(pat='SEDOL(.[^\:][a-zA-Z0-9_]*)')
    df['CUSIP'] = df.astype(str).sum(axis=1).str.extract(pat='CUSIP(.[^\:][a-zA-Z0-9_]*)')

    # lambda regex edit remove non numercial/letters from SEDOl
    df['SEDOL'] = df['SEDOL'].apply(lambda x: re.findall('[a-zA-Z0-9]+', str(x)))
    df['SEDOL'] = df['SEDOL'].str.get(0)

    df['CUSIP'] = df['CUSIP'].apply(
        lambda x: re.findall('[a-zA-Z0-9]+', str(x)))  # lambda regex edit remove non numercial/letters from CUSIP
    df['CUSIP'] = df['CUSIP'].str.get(0)

    # cleansing functions
    # remove null value column(s)
    df.dropna(axis='columns', how='all', thresh=None, subset=None, inplace=True)

    # if else like replacement
    df['SEDOL'] = df['SEDOL'].mask(df['SEDOL'] == 'nan', '')
    df['CUSIP'] = df['CUSIP'].mask(df['CUSIP'] == 'nan', '')

    # removes lines containing sedol from Fund Name
    df['Fund Name'] = df['Fund Name'].str.replace(r'SEDOL.*', '')
    df['Fund Name'] = df['Fund Name'].str.replace(r'CUSIP.*', '')

    # insert new columns
    df.insert(0, 'Region', '')
    df.insert(0, 'Type', '')

    # delete row up to condition (partnership)

    rows_to_drop = []  # array which will hold rows to delete
    type_val = ''  # value variable, will be used to populate new columns
    stop_delete = False  # flag
    # df.to_csv('C:/Maulen/csv/output/outputtry.csv', index=False)

    for i, row in df.iterrows():  # i, will correspond to the appropriate index row for insertion
        if 'Partnership/Joint Venture Interests' in df.at[
            i, 'Fund Name']:  # additional regex will be needed for multiple type values later!
            type_val = 'Partnership/Joint Venture Interests'
            stop_delete = True
            # break; #so escapes deletion once Partership found
        elif 'Value of Interest in Common' in df.at[i, 'Fund Name']:
            type_val = 'Value of Interest in Common'
            stop_delete = True
        elif not stop_delete:
            rows_to_drop.append(i)
        else:
            df.at[i, 'Type'] = type_val

    # cleaning
    for i, row in df.iterrows():
        if ('Total' in df.at[i, 'Fund Name'] or 'Global Region' in df.at[i, 'Fund Name'] or 'Asia' in df.at[
            i, 'Fund Name'] or 'Korea' in df.at[
            i, 'Fund Name']):  # check! and not df.at[i, 'Shares/Par Value '] or isnull()?
            rows_to_drop.append(i)

    # region extraction and population
    region_val = ''
    for i, row in df.iterrows():
        if 'United States' in df.at[i, 'Fund Name']:
            region_val = 'United States'
        elif 'KOREA' in df.at[i, 'Fund Name']:
            region_val = 'Korea'
        elif 'Emerging Markets' in df.at[i, 'Fund Name']:
            region_val = 'Emerging Markets'
        elif 'North America' in df.at[i, 'Fund Name']:
            region_val = 'North America'
        elif 'Asia Region' in df.at[i, 'Fund Name']:
            region_val = 'Asia'
        elif 'Global Region' in df.at[i, 'Fund Name']:
            region_val = 'Global'
        df.at[i, 'Region'] = region_val

    # cusip values position coorection (i-1 in this case). Later it is possible to extract CUSIP directly from Fund Name however position correction will still be needed
    # because of occasional double rows
    cusip_val = 'Nan'

    for i, row in df.iterrows():
        if not df.at[i, 'Fund Name']:
            df.at[i - 1, 'CUSIP'] = df.at[i, 'CUSIP']
            rows_to_drop.append(i)

    # drop step, when actual drop is executed
    dfDropped = df.drop(rows_to_drop)

    dfDropped.to_csv(out_file, index=False)  # export csv
    df = pd.read_csv(out_file)
    df.dropna(subset=['Shares/Par Value '], how='all', inplace=True)  # remove null value rows (Total, Subheadings)
    df.fillna('n/a')
    df.to_csv(out_file, index=False)

def combineFiles(path, out_file):
    all_files = glob.glob(
        os.path.join(path, "*_output.csv"))  # advisable to use os.path.join as this makes concatenation OS independent

    all_files.sort()

    df_from_each_file = (pd.read_csv(f) for f in all_files)
    concatenated_df = pd.concat(df_from_each_file, ignore_index=True)
    concatenated_df.to_csv(out_file, index=False)

def runFilesOneByOne(path, out_path):
    all_files = glob.glob(path + "/*.csv")
    all_files.sort()
    count = 0;
    for filename in all_files:
        out_file_name = out_path + str(count) + '_output.csv'
        populateDataFrameWithLogic( filename, out_file_name )
        count += 1

if __name__ == "__main__":

    path = sys.argv[1] # r'C:/Maulen/csv/input/' # use your path
    out_path = sys.argv[2]  #r'C:/Maulen/csv/output/'  # use your path

    #proceeiles one by one
    runFilesOneByOne(path, out_path)

    #combine into 1 csv
    combined_out_file = out_path +'combined.csv'
    combineFiles(out_path, combined_out_file)

    #removing left over files
    for hgx in glob.glob(path + "*_cleansed.csv"):
        os.remove(hgx)
    for hgx in glob.glob(out_path + "*_output.csv"):
        os.remove(hgx)


